package robot.message;

public class MessageObject {
    private Double modelingTime;
    private Double robotRadius;
    private Double robot_currentWeight;
    private Double robotCenterCoordinateX;
    private Double robotCenterCoordinateY;
    private Double robotVelocityCoordinateX;
    private Double robotVelocityCoordinateY;
    private Double taskRadius;
    private Double taskCenterCoordinateX;
    private Double taskCenterCoordinateY;
    private int currentTaskNumber;
    private int point;
    private int fail;
    private int crash;
    private Double fuel;
    private int done;
    private int error;
    private int ignored;

    public Double getModelingTime() {
        return modelingTime;
    }

    public void setModelingTime(Double modelingTime) {
        this.modelingTime = modelingTime;
    }

    public Double getRobotRadius() {
        return robotRadius;
    }

    public void setRobotRadius(Double robotRadius) {
        this.robotRadius = robotRadius;
    }

    public Double getRobot_currentWeight() {
        return robot_currentWeight;
    }

    public void setRobot_currentWeight(Double robot_currentWeight) {
        this.robot_currentWeight = robot_currentWeight;
    }

    public Double getRobotCenterCoordinateX() {
        return robotCenterCoordinateX;
    }

    public void setRobotCenterCoordinateX(Double robotCenterCoordinateX) {
        this.robotCenterCoordinateX = robotCenterCoordinateX;
    }

    public Double getRobotCenterCoordinateY() {
        return robotCenterCoordinateY;
    }

    public void setRobotCenterCoordinateY(Double robotCenterCoordinateY) {
        this.robotCenterCoordinateY = robotCenterCoordinateY;
    }

    public Double getRobotVelocityCoordinateX() {
        return robotVelocityCoordinateX;
    }

    public void setRobotVelocityCoordinateX(Double robotVelocityCoordinateX) {
        this.robotVelocityCoordinateX = robotVelocityCoordinateX;
    }

    public Double getRobotVelocityCoordinateY() {
        return robotVelocityCoordinateY;
    }

    public void setRobotVelocityCoordinateY(Double robotVelocityCoordinateY) {
        this.robotVelocityCoordinateY = robotVelocityCoordinateY;
    }

    public Double getTaskRadius() {
        return taskRadius;
    }

    public void setTaskRadius(Double taskRadius) {
        this.taskRadius = taskRadius;
    }

    public Double getTaskCenterCoordinateX() {
        return taskCenterCoordinateX;
    }

    public void setTaskCenterCoordinateX(Double taskCenterCoordinateX) {
        this.taskCenterCoordinateX = taskCenterCoordinateX;
    }

    public Double getTaskCenterCoordinateY() {
        return taskCenterCoordinateY;
    }

    public void setTaskCenterCoordinateY(Double taskCenterCoordinateY) {
        this.taskCenterCoordinateY = taskCenterCoordinateY;
    }

    public int getCurrentTaskNumber() {
        return currentTaskNumber;
    }

    public void setCurrentTaskNumber(int currentTaskNumber) {
        this.currentTaskNumber = currentTaskNumber;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getFail() {
        return fail;
    }

    public void setFail(int fail) {
        this.fail = fail;
    }

    public int getCrash() {
        return crash;
    }

    public void setCrash(int crash) {
        this.crash = crash;
    }

    public Double getFuel() {
        return fuel;
    }

    public void setFuel(Double fuel) {
        this.fuel = fuel;
    }

    public int getDone() {
        return done;
    }

    public void setDone(int done) {
        this.done = done;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public int getIgnored() {
        return ignored;
    }

    public void setIgnored(int ignored) {
        this.ignored = ignored;
    }
}
