package robot.message;

import java.text.ParseException;

public class MessageParser {

    public static MessageObject parse(String str) throws ParseException {
        String result = str.replaceAll("\\{\\(|\\)\\(|\\)\\}", " ");

        MessageObject messageObject = new MessageObject();
        
        for (String returnValue : result.split(" ")) {
            String[] paramArray = returnValue.split("=");
            if (paramArray.length > 1) {
                String key = paramArray[0];
                String value = paramArray[1];

                switch (key) {
                    case "T":
                        messageObject.setModelingTime(new Double(value));
                        break;
                    case "R":
                        messageObject.setRobotRadius(new Double(value));
                        break;
                    case "M":
                        messageObject.setRobot_currentWeight(new Double(value));
                        break;
                    case "RX":
                        messageObject.setRobotVelocityCoordinateY(new Double(value));
                        break;
                    case "RY":
                        messageObject.setRobotCenterCoordinateY(new Double(value));
                        break;
                    case "VRX":
                        messageObject.setRobotVelocityCoordinateX(new Double(value));
                        break;
                    case "VRY":
                        messageObject.setRobotVelocityCoordinateY(new Double(value));
                        break;
                    case "TR":
                        messageObject.setTaskRadius(new Double(value));
                        break;
                    case "TX":
                        messageObject.setTaskCenterCoordinateX(new Double(value));
                        break;
                    case "TY":
                        messageObject.setTaskCenterCoordinateY(new Double(value));
                        break;
                    case "TASK":
                        messageObject.setCurrentTaskNumber(new Integer(value));
                        break;
                    case "FAIL":
                        messageObject.setFail(new Integer(value));
                        break;
                    case "CRASH":
                        messageObject.setCrash(new Integer(value));
                        break;
                    case "FUEL":
                        messageObject.setFuel(new Double(value));
                        break;
                    case "DONE":
                        messageObject.setDone(new Integer(value));
                        break;
                    case "ERROR":
                        messageObject.setError(new Integer(value));
                        break;
                    case "IGNORED":
                        messageObject.setIgnored(new Integer(value));
                        break;
                }
            }
        }
        return messageObject;
    }
}

