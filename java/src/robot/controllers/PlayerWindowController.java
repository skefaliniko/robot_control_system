package robot.controllers;

import robot.gui.DrawPanel;
import robot.message.MessageObject;
import robot.message.MessageParser;
import robot.gui.GUI;

import javax.swing.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlayerWindowController {
    private List<String> messageStringList;
    private List<MessageObject> messageObjectList;

    private GUI gui;
    private DrawPanel drawPanel;


    public PlayerWindowController() {
        messageStringList = new ArrayList<>();
        messageObjectList = new ArrayList<>();
    }

    public void start() {

//        while (!messageObjectList.isEmpty()) {
//                applyParamsToGui(messageObjectList.get(0));
//                messageObjectList.remove(0);
//        }
    }

    public void sendMessageString(String messageString) {
        try {
            if (messageString != null) {
                MessageObject messageObject = new MessageParser().parse(messageString);
//                messageObjectList.add(messageObject);
                if (messageObject != null) {
                    messageObject = new MessageParser().parse("{(T=0.001000)(R=0.066700)(M=475.457764)(RX=0.044599)(RY=-0.110955)(VRX=-0.092654)(VRY=-0.089555)(TR=0.000000)(TX=0.000000)(TY=0.000000)(TASK=1)(POINT=0)(FAIL=0)(CHASH=0)(FUEL=332.820435)(DONE=0)(ERROR=0)(IGNORED=0)}");

                    applyParamsToGui(messageObject);
                } else {
//                applyParamsToGui(messageObject);
                }
            }
        } catch (Exception e) {
            System.out.println("Error with incoming message string");
            e.printStackTrace();
        }
    }

    private void applyParamsToGui(MessageObject messageObject) {
        if (messageObject != null) {
            drawPanel.setCurrX(messageObject.getRobotCenterCoordinateX());
            drawPanel.setCurrY(messageObject.getRobotCenterCoordinateY());
        } else {
            drawPanel.setCurrX(5.788);
            drawPanel.setCurrY(10.2);
        }

    }

    public String getCurrentCoordinates() {
        String resultMessage;
        Double[] arr = getCoupleCoordinatesArray();
        resultMessage= "{(FRX=" + arr[0] + ")(FRY=" + arr[1] + ")}";
        return resultMessage;
    }

    private Double[] getCoupleCoordinatesArray() {
        //..
        Double[] arrCoord = new Double[2];
        arrCoord[0] = drawPanel.getNextX();
        arrCoord[1] = drawPanel.getNextY();
        return arrCoord;
    }

}