package robot;

import robot.gui.GUI;
import java.io.IOException;
import java.text.ParseException;

public class Main {
    public static void main(String[] args) throws ParseException, IOException {
        GUI window = new GUI();
        window.start();
    }
}
