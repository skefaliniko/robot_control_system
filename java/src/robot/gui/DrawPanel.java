package robot.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DrawPanel extends JComponent {
    private double currX = 0;
    private double currY = 10;
    private double nextX = 0;
    private double nextY = 0;
    private int width = 10;
    private int height = 10;
    private static final int TIMER_DELAY = 100;

    public double getCurrX() {
        return currX;
    }

    public void setCurrX(double currX) {
        this.currX = currX;
    }

    public double getCurrY() {
        return currY;
    }

    public void setCurrY(double currY) {
        this.currY = currY;
    }

    public double getNextX() {
        return nextX;
    }

    public void setNextX(double nextX) {
        this.nextX = nextX;
    }

    public double getNextY() {
        return nextY;
    }

    public void setNextY(double nextY) {
        this.nextY = nextY;
    }

    public DrawPanel() {
//        SwingUtilities.invokeLater(new Runnable() {
//            @Override
//            public void run() {
        new Timer(TIMER_DELAY, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                nextX = currX + 0.2;
                nextY = currY - 0.3;
                repaint();
            }
        }).start();
//                nextX = currX + 0.2;
//                nextY = currY - 0.3;
//                repaint();
//            }
//        });
    }

    public void paintComponent(Graphics g) {
        g.setColor(Color.RED);
        g.fillOval((int)(currX * 10), (int) (currY*10), width, height);
    }
}
