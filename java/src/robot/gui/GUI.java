package robot.gui;

import robot.controllers.PlayerWindowController;
import robot.message.MessageObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Queue;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GUI {
    private DrawPanel drawPanel;
    private ParamPanel paramPanel;
    private JFrame windowFrame;

    private Queue<MessageObject> paramArray;

    private PlayerWindowController playerWindowController;
    private SocketChannel channel;

    public GUI() {

        playerWindowController = new PlayerWindowController();
    }

    public void start() {
        windowFrame = new JFrame("Robot control system");
        JButton startButton = new JButton("START");
        JButton stopButton = new JButton("EXIT");

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(startButton);
        buttonPanel.add(stopButton);

        drawPanel = new DrawPanel();
        paramPanel = new ParamPanel();

        drawPanel.setBackground(Color.BLACK);

        windowFrame.getContentPane().add(BorderLayout.CENTER, drawPanel);
        windowFrame.getContentPane().add(BorderLayout.SOUTH, buttonPanel);
        windowFrame.getContentPane().add(BorderLayout.EAST, paramPanel);

        windowFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        windowFrame.setSize(400, 400);
        windowFrame.setVisible(true);

        Thread socketProcessThread = new Thread(new Runnable() {
            @Override
            public void run() {
                startSocketProcess();
            }
        });

        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                socketProcessThread.start();
            }
        });

        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                socketProcessThread.stop();
                windowFrame.dispose();

            }
        });
    }

    private void startSocketProcess() {
        String HOST = "localhost";
        int PORT = 9999;
        int BUFFER_BYTE_SIZE = 215;

        StringBuffer stringBufferForCorruptedParts = new StringBuffer();

        try {
            channel = SocketChannel.open();
            channel.connect(new InetSocketAddress(HOST, PORT));
            Selector selector = Selector.open();
            channel.configureBlocking(false);
            channel.register(selector, SelectionKey.OP_READ);

            playerWindowController.start();

            while (true) {
                int readyChannels = selector.select();

                if (readyChannels == 0) {
                    continue;
                }

                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();

                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();

                    if (key.isReadable()) {//
                        ByteBuffer receivingBuffer = ByteBuffer.allocate(BUFFER_BYTE_SIZE);
                        receivingBuffer.clear();
                        String receivingMessage = "";
                        while ((channel.read(receivingBuffer)) > 0) {
                            receivingBuffer.flip();
                            receivingMessage += Charset.defaultCharset().decode(receivingBuffer);
                        }

//                        System.out.println("socket->term => " + receivingMessage);
                        String[] answer = getCorrectedInputMessage(receivingMessage, stringBufferForCorruptedParts.toString());
                        stringBufferForCorruptedParts.setLength(0);
                        String correctReceivingMessage = answer[0];
                        stringBufferForCorruptedParts.append(answer[1]);
                        System.out.println("Right " + correctReceivingMessage);
                        System.out.println("--------------------");
                        System.out.println("stringbuffer:=>" + stringBufferForCorruptedParts.toString());
//                        System.out.println(MessageParser.parse(receivingMessage).getRobotCenterCoordinateY());
//                        playerWindowController.sendMessageString(receivingMessage);

                        channel.register(selector, SelectionKey.OP_WRITE);
                    }

                    if (key.isWritable()) {
//                        String sendingMessage = playerWindowController.getCurrentCoordinates(); //????????
//
//                        ByteBuffer sendingBuffer = ByteBuffer.allocate(BUFFER_BYTE_SIZE);
//                        sendingBuffer.clear();
//                        sendingBuffer.put(sendingMessage.getBytes());
//                        sendingBuffer.flip();
//
//                        while (sendingBuffer.hasRemaining()) {
//                            int i = channel.write(sendingBuffer);
//                            System.out.println("DONE" + i);
//                        }

                        channel.register(selector, SelectionKey.OP_READ);
                    }
                    Thread.sleep(500);
                }
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String[] getCorrectedInputMessage(String inputMessage, String corruptedParts) {
        Pattern incomingMessageRegExp = Pattern.compile("\\{(\\([A-Z]{1,}\\=\\-?[1-9|0]{1,}(\\.[0-9]{1,})?\\)){1,}\\}");
        Matcher matcher = incomingMessageRegExp.matcher(inputMessage);
        String correctAnswer = "";
        StringBuffer corruptedTmpBuffer = new StringBuffer();
        if (((corruptedParts == null) || corruptedParts.equals("")) && matcher.find()) {
            correctAnswer = matcher.group(0);
            corruptedTmpBuffer.append(inputMessage.substring(correctAnswer.length(), inputMessage.length()));
        }
        if (((corruptedParts == null) || corruptedParts.equals("")) && !(matcher.find())) {
            corruptedTmpBuffer.append(inputMessage);
        }
        if (!((corruptedParts == null) || corruptedParts.equals("")) && matcher.find()) {
            correctAnswer = matcher.group(0);
            corruptedTmpBuffer.append(inputMessage.substring(correctAnswer.length(), inputMessage.length()));
        }
        if (!((corruptedParts == null) || corruptedParts.equals("")) && !(matcher.find())) {
            String stringBuffer = corruptedParts + inputMessage;
            Matcher matcherForCorrupted = incomingMessageRegExp.matcher(inputMessage);
            if (matcherForCorrupted.find()) {
                correctAnswer = matcherForCorrupted.group(0);
                corruptedTmpBuffer.append(stringBuffer.substring(correctAnswer.length(), stringBuffer.length() - 1)); //-1?
            } else {
                corruptedTmpBuffer.append(inputMessage);
            }
        }
//        if ((corruptedParts == null) || corruptedParts.equals("")) {
//            if (matcher.find()) {
//                correctAnswer = matcher.group(1);
//                corruptedTmpBuffer.append(inputMessage.substring(correctAnswer.length(), inputMessage.length()));
//            } else {
//                corruptedTmpBuffer.append(inputMessage);
//            }
//        } else {
//            String stringBuffer = corruptedParts + inputMessage;
//            Matcher matcherForCorrupted = incomingMessageRegExp.matcher(inputMessage);
//            if (matcherForCorrupted.find()) {
//                correctAnswer = matcherForCorrupted.group(1);
//                corruptedTmpBuffer.append(stringBuffer.substring(correctAnswer.length(), stringBuffer.length() - 1)); //-1?
//            } else {
//                corruptedTmpBuffer.append(inputMessage);
//            }
//        }
        return new String[]{correctAnswer, corruptedTmpBuffer.toString()};
    }

    private class ParamPanel extends JPanel {
    }
}
