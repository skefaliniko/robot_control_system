#This script parses single message like {(X=3.14)...} and extracts parameters as key & value to hash

class SingleMessageParser
  def SingleMessageParser.parse_message(input_message)    
    @@another_example = input_message.slice( /\(.*\)/ ).gsub!(/\(/, "").gsub!(/\)/, " ").split(" ") #every (..) parse to str
    param_keys_values = {}
    @@another_example.each { |param_string|
      #split params strings by = and add new parameter by key
      single_param_array = param_string.split("=")  
      param_keys_values[single_param_array[0].to_sym] = single_param_array[1].to_f
    }
    return param_keys_values
  end
end
