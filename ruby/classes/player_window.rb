require 'gosu'
require 'dish'
require_relative '../lib/z_order_module'

class PlayerWindow < Gosu::Window
  attr_accessor :current_x, :current_y, :next_x, :next_y, :modeling_time, :robot_radius, :current_robot_weight,
                :robot_center_X , :robot_center_Y, :robot_velocity_X , :robot_velocity_Y,
                :third_task_target_radius, :third_task_target_center_X, :third_task_target_center_Y, :task_number,
                :point_number, :fail, :crash, :fuel, :all_tasks_done, :msg_error, :msg_ignored,
                :msg_obj;
                
  SCREEN_HEIGHT = 1000
  SCREEN_WIDTH = 1000
  
  def initialize
    super(SCREEN_WIDTH, SCREEN_HEIGHT, false)
    self.caption = "Robo Game"    
    @background = Gosu::Image.new('img/stars-background.jpg')
    @current_x = 0
    @current_y = 0
    @next_x = 0
    @next_y = 0
    @msg_obj = 0
  end
  
  def set_modelling_to_image_coord(x_curr, y_curr)
    x_curr += 500
    y_curr += 500
    return x_curr, y_curr
  end
  
   def set_image_to_modelling_coord(x_curr, y_curr)
    x_curr -= 500
    y_curr -= 500
    return x_curr, y_curr
  end
  
  
  
  def start
    show
  end
  
  # def set_parameters_from_msg_obj(msg_obj)
    # @modeling_time = msg_obj.T      #далее просто присваиваем, а контролим в game_controller'e
    # @robot_radius = msg_obj.R
    # @current_robot_weight = msg_obj.M
    # @robot_center_X , @robot_center_Y = set_modelling_to_image_coord(msg_obj.RX, msg_obj.RY)
    # @robot_velocity_X , @robot_velocity_Y = set_modelling_to_image_coord(msg_obj.VRY, msg_obj.VRY)
    # @third_task_target_radius = msg_obj.TR
    # @third_task_target_center_X, @third_task_target_center_Y = set_modelling_to_image_coord(msg_obj.TX, msg_obj.TY)
    # @task_number = msg_obj.TASK
    # @point_number = msg_obj.POINT
    # @fail = msg_obj.FAIL
    # @crash = msg_obj.CRASH
    # @fuel = msg_obj.FUEl
    # @all_tasks_done = msg_obj.DONE
    # @msg_error = msg_obj.ERROR
    # @msg_ignored = msg_obj.IGNORED
#      
    # puts @robot_center_X , @robot_center_Y, @robot_velocity_X , @robot_velocity_Y
  # end
  
  def get_target_coordinates
    return set_image_to_modelling_coord(@next_x, @next_y)
  end

  def draw
    @scene_ready ||= true
    @background.draw(0,0,ZOrder::Background)
    draw_coordinates
    draw_line(@current_x,@current_y,Gosu::Color.argb(0xff_fff000),@next_x,@next_y,Gosu::Color.argb(0xff_fff000),z=ZOrder::Background,mode= :default)
  end
  
  def update
    # #constant speed you want the object to move at
    # delta_x = @target_x - @x
    # delta_y = @target_y - @y
    # 
    # # pythagoras to get the distance
    # # Using vectors works as well
    # # Gosu.distance would also work
    # goal_dist = Math.sqrt( (delta_x * delta_x) + (delta_y * delta_y) )
    # 
    # if (goal_dist > @speed_per_tick)
      # ratio = @speed_per_tick / goal_dist
      # x_move = ratio * delta_x
      # y_move = ratio * delta_y
      # @x += x_move
      # @y += y_move
    # else
      # @x = @target_x
      # @y = @target_y
    # end
    
     # draw_line(@current_x,@current_y,Gosu::Color.argb(0xff_fff000),@next_x,@next_y,Gosu::Color.argb(0xff_fff000),z=ZOrder::Background,mode= :default)    
     # puts @next_x = @current_x + 0.5
     # puts @next_y = @current_y + 0.6
     # set_parameters_from_msg_obj(@msg_obj)
     puts @next_x = @current_x + 0.5
     puts @next_y = @current_y + 0.6
     puts msg_obj
  end
  
  def draw_coordinates
    draw_line(0,500,Gosu::Color.argb(0xff_ffffff),1000,500,Gosu::Color.argb(0xff_ffffff),z=ZOrder::Background,mode= :default)
    draw_line(500,1000,Gosu::Color.argb(0xff_ffffff),500,0,Gosu::Color.argb(0xff_ffffff),z=ZOrder::Background,mode= :default)
    # draw_counted_dot(500, 500, 0xff_ff0000)
    draw_target_dot(100, 500, 0xff_00ff00)
    draw_target_dot(300, 600, 0xff_00ff00)
    draw_target_dot(100, 200, 0xff_00ff00)
    draw_target_dot(400, 700, 0xff_00ff00)
  end
  
  def needs_cursor?
    true
  end
  
  def needs_redraw?
    !@scene_ready
  end
  
  def draw_target_dot(x0, y0, color_hex)
    draw_line(x0-4,y0-4,Gosu::Color.argb(color_hex),x0+4,y0+4,Gosu::Color.argb(color_hex),z=ZOrder::Points,mode= :default)
    draw_line(x0-4,y0+4,Gosu::Color.argb(color_hex),x0+4,y0-4,Gosu::Color.argb(color_hex),z=ZOrder::Points,mode= :default)
    
    draw_line(x0-2,y0-4,Gosu::Color.argb(color_hex),x0+6,y0+4,Gosu::Color.argb(color_hex),z=ZOrder::Points,mode= :default)
    draw_line(x0-2,y0+4,Gosu::Color.argb(color_hex),x0+6,y0-2,Gosu::Color.argb(color_hex),z=ZOrder::Points,mode= :default)
    
    draw_line(x0-6,y0-4,Gosu::Color.argb(color_hex),x0+2,y0+4,Gosu::Color.argb(color_hex),z=ZOrder::Points,mode= :default)
    draw_line(x0-6,y0+4,Gosu::Color.argb(color_hex),x0+2,y0-4,Gosu::Color.argb(color_hex),z=ZOrder::Points,mode= :default)
  end
  
  def draw_counted_dot(x0, y0, color_hex)
    draw_line(x0-10,y0-10,Gosu::Color.argb(color_hex),x0+10,y0+10,Gosu::Color.argb(color_hex),z=ZOrder::Points,mode= :default)
    draw_line(x0-10,y0+10,Gosu::Color.argb(color_hex),x0+10,y0-10,Gosu::Color.argb(color_hex),z=ZOrder::Points,mode= :default)
    
    draw_line(x0-9,y0-10,Gosu::Color.argb(color_hex),x0+11,y0+10,Gosu::Color.argb(color_hex),z=ZOrder::Points,mode= :default)
    draw_line(x0-9,y0+10,Gosu::Color.argb(color_hex),x0+11,y0-10,Gosu::Color.argb(color_hex),z=ZOrder::Points,mode= :default)
    
    draw_line(x0-11,y0-10,Gosu::Color.argb(color_hex),x0+9,y0+10,Gosu::Color.argb(color_hex),z=ZOrder::Points,mode= :default)
    draw_line(x0-11,y0+10,Gosu::Color.argb(color_hex),x0+9,y0-10,Gosu::Color.argb(color_hex),z=ZOrder::Points,mode= :default)
  end
end