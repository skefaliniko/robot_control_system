require 'socket'
# require 'thread'
require 'dish'
require_relative 'single_message_parser'

class GameController
  
  HOSTNAME = 'anthome' #localhost
  PORT = 9999
  
  def initialize()
    # @player_window = PlayerWindow.new
    # @player_window = player_window 
    
    @s2p_reader, @s2p_writer = IO.pipe
    @p2s_reader, @p2s_writer = IO.pipe
  end
  
  def start_controller
    @socket = TCPSocket.open HOSTNAME, PORT
    begin
      start
    ensure
      stop
    end
  end
  
  def stop
    @socket.close
    # @player_window.close
  end
  
  def start
    
    fork do
      loop do
      
      #read data from socket, parse and send to pipe ServerToPlayer
      msg = @socket.gets("}")  #"}" is separator for getting instances of command messages
      puts "ToQueue-> #{msg}"
      @s2p_reader.close
      @s2p_writer.puts msg
      
      #read data from player, convert and send to pipe PlayerToServer
      @p2s_writer.close
      message_to_srv = @p2s_reader.gets
      @socket.write message_to_srv
      puts message_to_srv
      end
    end
    loop do
    #read data from pipe ServerToPlayer, convert and send to player window
    @s2p_writer.close
    value = @s2p_reader.gets
    msg_obj = convert_data_to_object(value)
    # player_window.msg_obj
    print "SendToGosu-> #{msg_obj}\n"
    
    #get coordinates from player, convert and send to pipe PlayerToSocket
    @p2s_reader.close
    msg = convert_coordinates_to_message(0.001, 0.45) #player_window.get_target_coordinates)
    puts "ToSRVQueue-> #{msg}"
    @p2s_writer.write msg
    end
  end
  
  #.....
  
  def game_over?
    unless player_window.fail == 1 || player_window.crash == 1 || player_window.all_tasks_done == 1
      return false
    else
      return true
    end         
  end
  
  def convert_data_to_object(string_message) #convert str->hash->obj
    return Dish SingleMessageParser.parse_message(string_message)
  end
  
  def convert_coordinates_to_message(next_x, next_y)
    return "{(FRX=#next_x)(FRY=#next_y)}"
  end
  
end